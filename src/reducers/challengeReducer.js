import * as actionTypes from "../actions/challengeActions";
import update from "immutability-helper";

const initialState = {
  dataTypes: [
    {
      type: "Portugal",
      data: [
        { name: "Data 1", type: "Portugal", checked: false },
        { name: "Data 2", type: "Portugal", checked: false },
        { name: "Data 3", type: "Portugal", checked: false },
        { name: "Data 4", type: "Portugal", checked: false },
      ],
    },
    {
      type: "Nicaragua",
      data: [
        { name: "Data 5", type: "Nicaragua", checked: false },
        { name: "Data 6", type: "Nicaragua", checked: false },
        { name: "Data 7", type: "Nicaragua", checked: false },
        { name: "Data 8", type: "Nicaragua", checked: false },
      ],
    },
    {
      type: "Marshal Islands",
      data: [
        { name: "Data 9", type: "Marshal Islands", checked: false },
        { name: "Data 10", type: "Marshal Islands", checked: false },
        { name: "Data 11", type: "Marshal Islands", checked: false },
        { name: "Data 12", type: "Marshal Islands", checked: false },
      ],
    },
  ],
  items: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    // phonenumber list
    case actionTypes.REQUEST_ALL_DATA:
      return {
        ...state,
        data: state.data,
      };
    case actionTypes.REQUEST_ADD_DATA:
      const index = state.items.findIndex(
        (item) => item.type === action.data.type
      );
      const dataTypeIndex = state.dataTypes.findIndex(
        (item) => item.type === action.data.type
      );
      const dataArrIndex = state.dataTypes[dataTypeIndex].data.findIndex(
        (data) => {
          return data.name === action.data.name;
        }
      );

      if (state.items.length === 0 || index === -1) {
        let arr = [];
        arr.push(action.data);
        let obj = { type: action.data.type, data: arr };

        return update(state, {
          items: { $push: [obj] },
          dataTypes: {
            [dataTypeIndex]: {
              data: {
                [dataArrIndex]: {
                  checked: { $set: true },
                },
              },
            },
          },
        });
      }
      if (index >= 0) {
        return update(state, {
          items: {
            [index]: {
              data: { $push: [action.data] },
            },
          },
          dataTypes: {
            [dataTypeIndex]: {
              data: {
                [dataArrIndex]: {
                  checked: { $set: true },
                },
              },
            },
          },
        });
      }

    case actionTypes.REQUEST_REMOVE_DATA:
      const inx = state.items.findIndex(
        (item) => item.type === action.data.type
      );

      const dataTypeInx = state.dataTypes.findIndex(
        (item) => item.type === action.data.type
      );
      const dataArrInx = state.dataTypes[dataTypeInx].data.findIndex((data) => {
        return data.name === action.data.name;
      });

      const itemInx = state.items.findIndex(
        (item) => item.type === action.data.type
      );

      const dataLength = state.items[itemInx].data.length;

      if (dataLength === 1) {
        return update(state, {
          items: {
            $set: state.items.filter((item) => {
              return item.type !== action.data.type;
            }),
          },
          dataTypes: {
            [dataTypeInx]: {
              data: {
                [dataArrInx]: {
                  checked: { $set: false },
                },
              },
            },
          },
        });
      }

      return update(state, {
        items: {
          [inx]: {
            data: {
              $set: state.items[inx].data.filter(
                (item) => item.name !== action.data.name
              ),
            },
          },
        },
        dataTypes: {
          [dataTypeInx]: {
            data: {
              [dataArrInx]: {
                checked: { $set: false },
              },
            },
          },
        },
      });

    default:
      return state;
  }
}
