import { combineReducers } from "redux";

import { connectRouter } from "connected-react-router";
import dataReducer from "./challengeReducer";

const rootReducer = (history) =>
  combineReducers({
    router: connectRouter(history),
    dataReducer,
  });

export default rootReducer;
