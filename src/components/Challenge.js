import React from "react";
import { connect } from "react-redux";
import { requestAddData, requestRemoveData } from "../actions/challengeActions";
import CountryUsers from "./CountryUsers";
import FilteredUsers from "./FilteredUsers";

class Challenge extends React.Component {
  handleChange = (event, data) => {
    const isChecked = event.target.checked;

    if (isChecked === true) {
      this.props.requestAddData(data);
    }
    if (isChecked === false) {
      this.props.requestRemoveData(data);
    }
  };
  render() {
    const { data, items, dataTypes } = this.props.data;

    return (
      <>
        <div id="wrap">
          <div className="leftBox">
            {dataTypes.map((data, i) => {
              return (
                <div key={i}>
                  <h2>{data.type}</h2>
                  <CountryUsers countryUsers={data} />
                </div>
              );
            })}
          </div>
          <div className="rightBox">
            {items.length === 0 ? (
              <div className="no-value">
                <p>NO value selected</p>
              </div>
            ) : (
              <>
                {items.map((item, i) => {
                  return (
                    <div key={i}>
                      <h2>{item.type}</h2>
                      <FilteredUsers filteredUsers={item} />
                    </div>
                  );
                })}
              </>
            )}
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.dataReducer,
  };
};

export default connect(mapStateToProps, { requestAddData, requestRemoveData })(
  Challenge
);
