import React from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Container, Row, Col, Form } from "react-bootstrap";

import * as Yup from "yup";

const ValidPhoneSchema = Yup.object().shape({});

class Reports extends React.Component {
  state = {
    startDate: new Date(),
    endDate: new Date(),
    phone: "",
    productId: "all"
  };

  handleStartDateChange = date => {
    this.setState({
      startDate: date
    });
  };

  handleEndDateChange = date => {
    this.setState({
      endDate: date
    });
  };

  handlePhoneNumberChange = e => {
    this.setState({
      phone: e.target.value
    });
  };
  handleBrandChange = event => {
    this.setState({ productId: event.target.value });
  };

  generateReport = e => {
    e.preventDefault();
    console.log(this.state);
  };

  render() {
    return (
      <>
        <Container>
          <Row>
            <Col sm={{ span: 6, offset: 3 }}>
              <h5 className="element-header">Reports</h5>
              <Form>
                <div className="form-group">
                  <label>Start Date</label>
                  <DatePicker
                    selected={this.state.startDate}
                    onChange={this.handleStartDateChange}
                    showTimeSelect
                    timeFormat="HH:mm"
                    timeIntervals={15}
                    timeCaption="time"
                    dateFormat="MMMM d, yyyy h:mm aa"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <label>End Date</label>
                  <DatePicker
                    selected={this.state.endDate}
                    onChange={this.handleEndDateChange}
                    showTimeSelect
                    timeFormat="HH:mm"
                    timeIntervals={15}
                    timeCaption="time"
                    dateFormat="MMMM d, yyyy h:mm aa"
                    className="form-control asd"
                  />
                </div>
                <div className="form-group">
                  <label>Phone Number</label>
                  <input
                    type="text"
                    className="form-control"
                    onChange={this.handlePhoneNumberChange}
                  />
                </div>
                <div class="form-group">
                  <label for="inputState">Select Brand</label>
                  <select id="inputState" class="form-control">
                    <option
                      value={this.state.productId}
                      onChange={this.handleBrandChange}
                    >
                      All
                    </option>
                    <option value="123">Amazon</option>
                    <option value="124">Flipkart</option>
                  </select>
                </div>
                <button
                  type="submit"
                  className="btn btn-primary btn-block"
                  onClick={this.generateReport}
                >
                  Generate Report
                </button>
              </Form>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default Reports;
