import React from "react";
import { connect } from "react-redux";
import { EjNavbar } from "../EjNavbar";
import {
  Navbar,
  Nav,
  Container,
  Row,
  Col,
  Card,
  Button,
  Alert,
  Spinner,
  Dropdown
} from "react-bootstrap";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { addMoneyToCard } from "../../actions/cardActions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { deleteFlashMessage } from "../../actions/flashMsgActions";
import { fetchPaymentId } from "../../actions/paymentActions";

const LoadResellerMoneySchema = Yup.object().shape({
  amount: Yup.number().required("Required"),
  transactionType: Yup.string().required("Transaction Type is required!"),
  //   brandType: Yup.string().required("Brand Type is required!"),
  notes: Yup.string(),
  transactionId: Yup.string().required("Transaction ID is required"),
  adjustmentAmount: Yup.number().required("Required")
});

class LoadResellerMoney extends React.Component {
  closeAlert = () => {
    this.props.deleteFlashMessage();
  };

  addMoney = async (value, card) => {
    let paymentId;
    try {
      paymentId = await this.props.fetchPaymentId({
        accessCode: this.props.user.accessCode
      });
    } catch (e) {
      console.log(e);
    }

    // fetchPaymentId

    const apiObj = {
      amount: value.amount,
      transactionType: value.transactionType,
      transactionId: value.transactionId,
      notes: value.notes,
      adjustmentAmount: value.adjustmentAmount,
      ref_no: paymentId.paymentId,
      cardId: card.cardId,
      product_id: card.productId,
      phonenumber: card.phonenumber,
      accessCode: this.props.user.accessCode
    };

    await this.props.addMoneyToCard(apiObj);
  };
  render() {
    const cardId = this.props.location.substring(1);
    const card = this.props.cards.cards.find(card => card.cardId === cardId);
    // data.find(p => p.name === match.params.resID);

    if (card) {
      return (
        <>
          <Container>
            <Row>
              <Col sm={{ span: 6, offset: 3 }} style={{ marginTop: 30 }}>
                {this.props.flashMsg.flashMsg !== "" &&
                this.props.flashMsg.variant !== "" ? (
                  <Alert variant={this.props.flashMsg.variant}>
                    {this.props.flashMsg.flashMsg}
                    <span style={{ float: "right" }} onClick={this.closeAlert}>
                      <FontAwesomeIcon icon={faTimes} size="1x" />
                    </span>
                  </Alert>
                ) : null}

                {card.productId === 9142 ? (
                  <h2 style={{ fontWeight: "600" }}>AMAZON</h2>
                ) : null}
                {card.productId === 9146 ? (
                  <h2 style={{ fontWeight: "600" }}>FLIPKART</h2>
                ) : null}

                <Formik
                  initialValues={{
                    amount: 0,
                    transactionType: "",
                    notes: "",
                    brandType: "",
                    transactionId: "",
                    adjustmentAmount: 0
                  }}
                  validationSchema={LoadResellerMoneySchema}
                  onSubmit={value => this.addMoney(value, card)}
                  //   onSubmit={value => {
                  //   const apiObj = {
                  //     amount: value.amount,
                  //     transactionType: value.transactionType,
                  //     brandType: value.brandType,
                  //     transactionId: value.transactionId,
                  //     notes: value.notes,
                  //     ref_no: card.ref_no,
                  //     cardId: card.cardId,
                  //     product_id: card.productId,
                  //     phonenumber: card.phonenumber,
                  //     accessCode: this.props.user.accessCode
                  //   };

                  //     this.props.addMoneyToCard(apiObj);
                  //   }}
                >
                  {({
                    touched,
                    errors,
                    isSubmitting,
                    values,
                    handleChange,
                    handleBlur
                  }) => (
                    <Form>
                      <div className="form-group">
                        <label>Transaction Type</label>
                        <Field
                          as="select"
                          name="transactionType"
                          value={values.transactionType}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          className={`form-control ${
                            touched.transactionType && errors.transactionType
                              ? "is-invalid"
                              : ""
                          }`}
                        >
                          <option value="" label="Select a transactionType">
                            Select a transactionType
                          </option>
                          <option value="RAZORPAY_TRANSFER" label="Razorpay">
                            Razorpay
                          </option>
                          <option value="BANK_TRANSFER" label="Bank Transfer">
                            Bank Transfer
                          </option>
                          <option
                            value="BALANCE_ADJUSTMENT"
                            label="Balance Adjustment"
                          >
                            Balance Adjustment
                          </option>
                        </Field>

                        {errors.transactionType && touched.transactionType && (
                          <ErrorMessage
                            component="div"
                            name="transactionType"
                            className="invalid-feedback"
                          />
                        )}

                        {/* <label>Amount</label>
                        <Field
                          type="text"
                          name="amount"
                          placeholder="Enter Amount"
                          className={`form-control ${
                            touched.amount && errors.amount ? "is-invalid" : ""
                          }`}
                        />
                        <ErrorMessage
                          component="div"
                          name="amount"
                          className="invalid-feedback"
                        /> */}

                        {values.transactionType === "BALANCE_ADJUSTMENT" ? (
                          <>
                            <label>Adjustment Amount</label>
                            <Field
                              type="text"
                              name="adjustmentAmount"
                              placeholder="Enter adjustmentAmount"
                              className={`form-control ${
                                touched.adjustmentAmount &&
                                errors.adjustmentAmount
                                  ? "is-invalid"
                                  : ""
                              }`}
                            />
                            <ErrorMessage
                              component="div"
                              name="adjustmentAmount"
                              className="invalid-feedback"
                            />
                          </>
                        ) : (
                          <>
                            <label>Amount</label>
                            <Field
                              type="text"
                              name="amount"
                              placeholder="Enter Amount"
                              className={`form-control ${
                                touched.amount && errors.amount
                                  ? "is-invalid"
                                  : ""
                              }`}
                            />
                            <ErrorMessage
                              component="div"
                              name="amount"
                              className="invalid-feedback"
                            />
                          </>
                        )}

                        {/* <label>Brand Type</label>
                        <Field
                          as="select"
                          name="brandType"
                          value={values.brandType}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          className={`form-control ${
                            touched.brandType && errors.brandType
                              ? "is-invalid"
                              : ""
                          }`}
                        >
                          <option value="" label="Select a brandType">
                            Select a brandType
                          </option>
                          <option value="FLIPKART" label="Flipkart">
                            Flipkart
                          </option>
                          <option value="AMAZON" label="Amazon">
                            Amazon
                          </option>
                        </Field> */}
                        {/* 
                        {errors.brandType && touched.brandType && (
                          <ErrorMessage
                            component="div"
                            name="brandType"
                            className="invalid-feedback"
                          />
                        )} */}

                        <div className="form-group">
                          <label>Transaction ID</label>
                          <Field
                            type="text"
                            name="transactionId"
                            placeholder="Enter Transaction ID"
                            className={`form-control ${
                              touched.transactionId && errors.transactionId
                                ? "is-invalid"
                                : ""
                            }`}
                          />
                          <ErrorMessage
                            component="div"
                            name="transactionId"
                            className="invalid-feedback"
                          />
                        </div>

                        <div className="form-group">
                          <label>Notes</label>
                          <Field
                            as="textarea"
                            type="text"
                            name="notes"
                            placeholder="Enter Note here"
                            className={`form-control ${
                              touched.notes && errors.notes ? "is-invalid" : ""
                            }`}
                          />
                        </div>

                        <button
                          type="submit"
                          className="btn btn-primary btn-block"
                          disabled={isSubmitting}
                        >
                          Submit
                        </button>
                      </div>
                    </Form>
                  )}
                </Formik>

                <Card
                  style={{
                    backgroundColor: "#34495e",
                    color: "#ecf0f1"
                  }}
                >
                  <Card.Body>
                    <Card.Subtitle
                      className="mb-2 text-muted"
                      style={{ fontSize: "0.8rem" }}
                    >
                      phonenumber: {card.phonenumber}
                    </Card.Subtitle>
                    <Card.Text style={{ fontWeight: "600" }}>
                      Reference Number: {card.ref_no}
                    </Card.Text>
                    <div>
                      <Row>
                        <Col>
                          <Card.Text>CardID: {card.cardId}</Card.Text>
                        </Col>
                        <Col>
                          <Card.Text>ProductID: {card.productId}</Card.Text>
                        </Col>
                      </Row>
                    </div>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Container>
        </>
      );
    } else {
      return <></>;
    }
  }
}

//export default LoadResellerMoney;

const mapStateToProps = state => {
  return {
    user: state.user,
    cards: state.cards,
    phoneNumbers: state.phoneNumbers,
    location: state.router.location.pathname,
    flashMsg: state.flashMsg
  };
};

export default connect(mapStateToProps, {
  addMoneyToCard,
  deleteFlashMessage,
  fetchPaymentId
})(LoadResellerMoney);

// import React from "react";
// import { connect } from "react-redux";
// import { EjNavbar } from "../EjNavbar";
// import {
//   Navbar,
//   Nav,
//   Container,
//   Row,
//   Col,
//   Card,
//   Button,
//   Alert,
//   Spinner,
//   Dropdown
// } from "react-bootstrap";
// import { Formik, Field, Form, ErrorMessage } from "formik";
// import * as Yup from "yup";
// import { addMoneyToCard } from "../../actions/cardActions";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faTimes } from "@fortawesome/free-solid-svg-icons";
// import { deleteFlashMessage } from "../../actions/flashMsgActions";
// import { fetchPaymentId } from "../../actions/paymentActions";

// const LoadResellerMoneySchema = Yup.object().shape({
//   amount: Yup.number()
//     .min(10, "Amount must be 10 or more than 10")
//     .required("Required"),
//   transactionType: Yup.string().required("Transaction Type is required!"),
//   brandType: Yup.string().required("Brand Type is required!"),
//   notes: Yup.string(),
//   transactionId: Yup.string().required("Transaction ID is required"),
//   adjustmentAmount: Yup.number()
//     .min(10, "Amount must be 10 or more than 10")
//     .required("Required")
// });

// class LoadResellerMoney extends React.Component {
//   closeAlert = () => {
//     this.props.deleteFlashMessage();
//   };

//   addMoney = async (value, card) => {
//     let paymentId;
//     try {
//       paymentId = await this.props.fetchPaymentId({
//         accessCode: this.props.user.accessCode
//       });
//     } catch (e) {
//       console.log(e);
//     }

//     // fetchPaymentId

//     const apiObj = {
//       amount: value.amount,
//       transactionType: value.transactionType,
//       brandType: value.brandType,
//       transactionId: value.transactionId,
//       notes: value.notes,
//       adjustmentAmount: value.adjustmentAmount,
//       ref_no: paymentId.paymentId,
//       cardId: card.cardId,
//       product_id: card.productId,
//       phonenumber: card.phonenumber,
//       accessCode: this.props.user.accessCode
//     };
//     console.log(apiObj);
//     await this.props.addMoneyToCard(apiObj);
//   };
//   render() {
//     const cardId = this.props.location.substring(1);
//     const card = this.props.cards.cards.find(card => card.cardId === cardId);
//     // data.find(p => p.name === match.params.resID);

//     if (card) {
//       return (
//         <>
//           <Container>
//             <Row>
//               <Col sm={{ span: 6, offset: 3 }} style={{ marginTop: 30 }}>
//                 {this.props.flashMsg.flashMsg !== "" &&
//                 this.props.flashMsg.variant !== "" ? (
//                   <Alert variant={this.props.flashMsg.variant}>
//                     {this.props.flashMsg.flashMsg}
//                     <span style={{ float: "right" }} onClick={this.closeAlert}>
//                       <FontAwesomeIcon icon={faTimes} size="1x" />
//                     </span>
//                   </Alert>
//                 ) : null}
//                 <Formik
//                   initialValues={{ amount: 0, transactionType: "", notes: "" }}
//                   validationSchema={LoadResellerMoneySchema}
//                   onSubmit={value => this.addMoney(value, card)}
//                   //   onSubmit={value => {
//                   //   const apiObj = {
//                   //     amount: value.amount,
//                   //     transactionType: value.transactionType,
//                   //     brandType: value.brandType,
//                   //     transactionId: value.transactionId,
//                   //     notes: value.notes,
//                   //     ref_no: card.ref_no,
//                   //     cardId: card.cardId,
//                   //     product_id: card.productId,
//                   //     phonenumber: card.phonenumber,
//                   //     accessCode: this.props.user.accessCode
//                   //   };

//                   //     this.props.addMoneyToCard(apiObj);
//                   //   }}
//                 >
//                   {({
//                     touched,
//                     errors,
//                     isSubmitting,
//                     values,
//                     handleChange,
//                     handleBlur
//                   }) => (
//                     <Form>
//                       <div className="form-group">
//                         <label>Amount</label>
//                         <Field
//                           type="text"
//                           name="amount"
//                           placeholder="Enter Amount"
//                           className={`form-control ${
//                             touched.amount && errors.amount ? "is-invalid" : ""
//                           }`}
//                         />
//                         <ErrorMessage
//                           component="div"
//                           name="amount"
//                           className="invalid-feedback"
//                         />
//                       </div>

//                       <label>Transaction Type</label>
//                       <Field
//                         as="select"
//                         name="transactionType"
//                         value={values.transactionType}
//                         onChange={handleChange}
//                         onBlur={handleBlur}
//                         className={`form-control ${
//                           touched.transactionType && errors.transactionType
//                             ? "is-invalid"
//                             : ""
//                         }`}
//                       >
//                         <option value="" label="Select a transactionType" />
//                         <option value="RAZORPAY_TRANSFER" label="Razorpay" />
//                         <option value="BANK_TRANSFER" label="Bank Transfer" />
//                         <option
//                           value="BALANCE_ADJUSTMENT"
//                           label="Balance Adjustment"
//                         />
//                       </Field>

//                       {/* <label>adjust amount</label> */}
//                       {values.transactionType === "BALANCE_ADJUSTMENT" && (
//                         <>
//                           <label>Balance adjustment amount</label>
//                           <Field
//                             type="text"
//                             name="adjustmentAmount"
//                             placeholder="Enter Adjustment Amount"
//                             className={`form-control ${
//                               touched.adjustmentAmount &&
//                               errors.adjustmentAmount
//                                 ? "is-invalid"
//                                 : ""
//                             }`}
//                           />
//                         </>
//                       )}

//                       {errors.transactionType && touched.transactionType && (
//                         <ErrorMessage
//                           component="div"
//                           name="transactionType"
//                           className="invalid-feedback"
//                         />
//                       )}

//                       <label>Brand Type</label>
//                       <Field
//                         as="select"
//                         name="brandType"
//                         value={values.brandType}
//                         onChange={handleChange}
//                         onBlur={handleBlur}
//                         className={`form-control ${
//                           touched.brandType && errors.brandType
//                             ? "is-invalid"
//                             : ""
//                         }`}
//                       >
//                         <option value="" label="Select a brandType" />
//                         <option value="FLIPKART" label="Flipkart" />
//                         <option value="AMAZON" label="Amazon" />
//                       </Field>

//                       {errors.brandType && touched.brandType && (
//                         <ErrorMessage
//                           component="div"
//                           name="brandType"
//                           className="invalid-feedback"
//                         />
//                       )}

//                       <div className="form-group">
//                         <label>Transaction ID</label>
//                         <Field
//                           type="text"
//                           name="transactionId"
//                           placeholder="Enter Transaction ID"
//                           className={`form-control ${
//                             touched.transactionId && errors.transactionId
//                               ? "is-invalid"
//                               : ""
//                           }`}
//                         />
//                         <ErrorMessage
//                           component="div"
//                           name="transactionId"
//                           className="invalid-feedback"
//                         />
//                       </div>

//                       <div className="form-group">
//                         <label>Notes</label>
//                         <Field
//                           as="textarea"
//                           type="text"
//                           name="notes"
//                           placeholder="Enter Note here"
//                           className={`form-control ${
//                             touched.notes && errors.notes ? "is-invalid" : ""
//                           }`}
//                         />
//                       </div>

//                       <button
//                         type="submit"
//                         className="btn btn-primary btn-block"
//                         disabled={isSubmitting}
//                       >
//                         Submit
//                       </button>
//                     </Form>
//                   )}
//                 </Formik>

//                 <Card
//                   style={{
//                     backgroundColor: "#34495e",
//                     color: "#ecf0f1"
//                   }}
//                 >
//                   <Card.Body>
//                     <Card.Subtitle
//                       className="mb-2 text-muted"
//                       style={{ fontSize: "0.8rem" }}
//                     >
//                       phonenumber: {card.phonenumber}
//                     </Card.Subtitle>
//                     <Card.Text style={{ fontWeight: "600" }}>
//                       Reference Number: {card.ref_no}
//                     </Card.Text>
//                     <div>
//                       <Row>
//                         <Col>
//                           <Card.Text>CardID: {card.cardId}</Card.Text>
//                         </Col>
//                         <Col>
//                           <Card.Text>ProductID: {card.productId}</Card.Text>
//                         </Col>
//                       </Row>
//                     </div>
//                   </Card.Body>
//                 </Card>
//               </Col>
//             </Row>
//           </Container>
//         </>
//       );
//     } else {
//       return <></>;
//     }
//   }
// }

// //export default LoadResellerMoney;

// const mapStateToProps = state => {
//   return {
//     user: state.user,
//     cards: state.cards,
//     phoneNumbers: state.phoneNumbers,
//     location: state.router.location.pathname,
//     flashMsg: state.flashMsg
//   };
// };

// export default connect(mapStateToProps, {
//   addMoneyToCard,
//   deleteFlashMessage,
//   fetchPaymentId
// })(LoadResellerMoney);
