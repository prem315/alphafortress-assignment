import React from "react";
import { connect } from "react-redux";
import Table from "react-bootstrap/Table";
import {
  Navbar,
  Nav,
  Container,
  Row,
  Col,
  Card,
  Button,
  Form,
  ListGroup,
  FormControl,
  Alert,
  Spinner
} from "react-bootstrap";

import { getResellerActivity } from "../../actions/resellerActivityActions";
import ResellerActivity from "./ResellerActivity";
import "../../styles/resellerActivity.scss";

class UserActivity extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    // this.props.getResellerActivity({
    //   accessCode: this.props.user.accessCode,
    //   phone: this.props.user.phoneNumber
    // });
  }

  render() {
    const reseller = this.props.data.find(
      reseller => reseller.phonenumber === this.props.match.params.phone
    );

    const brandcard = this.props.data.find(
      reseller =>
        reseller.productId === parseInt(this.props.match.params.brandType)
    );

    if (reseller) {
      return (
        <>
          <h2 style={{ textAlign: "center", marginTop: "15px" }}>
            Reseller Activity: {reseller.phonenumber}
          </h2>
          <ResellerActivity data={reseller} brandcard={brandcard} />
        </>
      );
    } else {
      return <div>loading</div>;
    }
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    resellerActivity: state.resellerActivity
  };
};

export default connect(mapStateToProps, { getResellerActivity })(UserActivity);

// <Container>
//         <Row>
//           <Col sm={{ span: 6, offset: 3 }} style={{ marginTop: 30 }}>
//             <h2 style={{ textAlign: "center" }}>Reseller Activity</h2>
//             {!loading ? (
//               <>
//                 {reseller_activity.map(activity => {
//                   return (
//                     <>
//                       <Card>
//                         <Card.Body>
//                           <Card.Subtitle
//                             className="mb-2 text-muted"
//                             style={{ fontSize: "0.8rem" }}
//                           >
//                             date{" "}
//                             {new Date(
//                               parseInt(activity.activitytime)
//                             ).toLocaleString()}{" "}
//                           </Card.Subtitle>
//                           <Card.Text style={{ fontWeight: "600" }}>
//                             {activity.message}
//                           </Card.Text>
//                           <div>
//                             <Row>
//                               <Col>
//                                 <Card.Text>{activity.eventType}</Card.Text>
//                               </Col>
//                               <Col>
//                                 <Card.Text>{activity.type}</Card.Text>
//                               </Col>
//                             </Row>
//                           </div>
//                         </Card.Body>
//                       </Card>
//                     </>
//                   );
//                 })}
//               </>
//             ) : (
//               <div
//                 style={{
//                   display: "flex",
//                   alignItems: "center",
//                   justifyContent: "center"
//                 }}
//               >
//                 <Spinner animation="border" role="status">
//                   <span className="sr-only">Loading...</span>
//                 </Spinner>
//               </div>
//             )}
//           </Col>
//         </Row>
//       </Container>
