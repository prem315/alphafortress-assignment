import React from "react";
import { connect } from "react-redux";
import { ListGroup, Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
// import { CopyToClipboard } from "react-copy-to-clipboard";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMobile } from "@fortawesome/free-solid-svg-icons";

class ResellerCard extends React.Component {
  render() {
    return (
      <>
        <Card className="resellerCard">
          {this.props.card.productId === 9142 ? (
            <img
              src="https://static.echojoy.in/logos/amazon.png"
              class="rounded"
            />
          ) : (
            <img
              src="https://static.echojoy.in/logos/flipkart.png"
              class="rounded"
            />
          )}

          {/* https://static.echojoy.in/logos/flipkart.png */}
          <Card.Body>
            <Card.Text style={{ fontWeight: "600" }}>
              <span>
                {new Date(parseInt(this.props.card.orderedAt)).toLocaleString()}
              </span>

              <span style={{ marginLeft: "10px" }}>
                {this.props.card.pin_or_url}
              </span>

              <span style={{ float: "right" }}>
                &#8377;{this.props.card.card_balance}
              </span>
            </Card.Text>
          </Card.Body>
          <hr />
          <Card.Link href="#" className="addMoney-btn">
            <Link
              to={`${this.props.router.location.pathname}${this.props.card.cardId}`}
            >
              Add Money
            </Link>
          </Card.Link>

          <Card.Link href="#" className="addMoney-btn">
            <Link
              to={`${this.props.router.location.pathname}resellercardsbalances/reselleractivity/${this.props.card.phonenumber}/${this.props.card.productId}`}
            >
              Balances
            </Link>
          </Card.Link>
        </Card>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    cards: state.cards,
    phoneNumbers: state.phoneNumbers,
    router: state.router
    // loading: state.phoneNumbers.loading
  };
};

export default connect(mapStateToProps, null)(ResellerCard);
