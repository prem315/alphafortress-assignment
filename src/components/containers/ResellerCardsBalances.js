import React from "react";
import { connect } from "react-redux";
import Table from "react-bootstrap/Table";
import {
  Container,
  Row,
  Col,
  Alert,
  Spinner,
  Card,
  Pagination
} from "react-bootstrap";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Route, Switch, Redirect, Link } from "react-router-dom";
import { getResellersCardsBalances } from "../../actions/resellersCardsBalancesActions";
import { getCards } from "../../actions/cardActions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMobile } from "@fortawesome/free-solid-svg-icons";
import ResellerCardBalance from "./ResellerCardBalance";
import ResellerCardActivity from "./ResellerCardActivity";
import UserActivity from "./UserActivity";
import { indianPhoneRegexValidation } from "../../utils/regx";

const ValidPhoneSchema = Yup.object().shape({
  phone: Yup.string().matches(
    indianPhoneRegexValidation,
    "Phone number is not valid"
  )
});

class ResellerCardsBalances extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  getCards = () => {
    const { loading, cards } = this.props.cards;
    return (
      <>
        <Container>
          <Row>
            <Col sm={{ span: 6, offset: 3 }} style={{ marginTop: 30 }}>
              <Formik
                initialValues={{ phone: "" }}
                validationSchema={ValidPhoneSchema}
                onSubmit={value => {
                  this.props.getCards({
                    access_token: this.props.user.accessCode,
                    number: value.phone,
                    pageNumber: 1
                  });
                }}
              >
                {({ touched, errors, isSubmitting }) => (
                  <Form>
                    <div className="form-group">
                      <label>Phone Number</label>
                      <Field
                        type="text"
                        name="phone"
                        placeholder="Enter Valid PhoneNumber"
                        className={`form-control ${
                          touched.phone && errors.phone ? "is-invalid" : ""
                        }`}
                      />
                      <ErrorMessage
                        component="div"
                        name="phone"
                        className="invalid-feedback"
                      />
                    </div>

                    <button type="submit" className="btn btn-primary btn-block">
                      Submit
                    </button>
                  </Form>
                )}
              </Formik>

              {loading === true ? (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Spinner animation="border" role="status">
                    <span className="sr-only">Loading...</span>
                  </Spinner>
                </div>
              ) : null}

              {loading === false ? (
                <>
                  {cards.map(card => {
                    return <ResellerCardActivity card={card} />;
                  })}
                  {/* <Pagination>{this.paginationItems()}</Pagination> */}
                </>
              ) : null}

              {this.props.flashMsg.flashMsg !== "" &&
              this.props.flashMsg.variant !== "" ? (
                <Alert variant={this.props.flashMsg.variant}>
                  {this.props.flashMsg.flashMsg}
                </Alert>
              ) : null}
            </Col>
          </Row>
        </Container>
      </>
    );
  };

  render() {
    return (
      <Container>
        <Switch>
          <Route
            exact
            path="/resellercardsbalances"
            component={this.getCards}
          />

          <Route
            path="/resellercardsbalances/resellercard/:phone"
            render={props => (
              <ResellerCardBalance
                data={this.props.resellerCardsBalances.balances}
                {...props}
              />
            )}
          />

          <Route
            path="/resellercardsbalances/reselleractivity/:phone/:brandType"
            render={props => (
              <UserActivity data={this.props.cards.cards} {...props} />
            )}
          />
        </Switch>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    resellerCardsBalances: state.resellerCardsBalances,
    cards: state.cards,
    flashMsg: state.flashMsg,
    router: state.router
  };
};

export default connect(mapStateToProps, {
  getResellersCardsBalances,
  getCards
})(ResellerCardsBalances);

// import React from "react";
// import { connect } from "react-redux";
// import Table from "react-bootstrap/Table";
// import {
//   Container,
//   Row,
//   Col,
//   Alert,
//   Spinner,
//   Card,
//   Pagination
// } from "react-bootstrap";

// import { Route, Switch, Redirect, Link } from "react-router-dom";
// import { getResellersCardsBalances } from "../../actions/resellersCardsBalancesActions";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faMobile } from "@fortawesome/free-solid-svg-icons";
// import ResellerCardBalance from "./ResellerCardBalance";
// import UserActivity from "./UserActivity";

// class ResellerCardsBalances extends React.Component {
//   constructor(props) {
//     super(props);

//     this.state = {};
//   }

//   componentDidMount() {
//     this.props.getResellersCardsBalances({
//       accessCode: this.props.user.accessCode,
//       pageNumber: 1
//     });
//   }

//   getResellerCardsPage = pageNumber => {
//     this.props.getResellersCardsBalances({
//       accessCode: this.props.user.accessCode,
//       pageNumber: pageNumber
//     });
//   };

//   paginationItems = () => {
//     const {
//       page,
//       size,
//       totalCount,
//       balances
//     } = this.props.resellerCardsBalances;
//     let totalPages = Math.ceil(totalCount / 10);
//     let active = page;
//     let items = [];

//     for (let number = 1; number <= totalPages; number++) {
//       if (balances.length !== 0) {
//         items.push(
//           <Pagination.Item
//             key={number}
//             active={number == active}
//             onClick={() => this.getResellerCardsPage(number)}
//           >
//             {number}
//           </Pagination.Item>
//         );
//       }
//     }
//     return <>{items}</>;
//   };

//   getResellerCards = () => {
//     const { loading, balances } = this.props.resellerCardsBalances;

//     if (balances.length === 0) {
//       return <p>No cards</p>;
//     } else {
//       return (
//         <>
//           <Row>
//             <Col sm={{ span: 6, offset: 3 }} style={{ marginTop: 30 }}>
//               {balances.map(reseller => {
//                 return (
//                   <>
//                     <Card>
//                       <Card.Body>
//                         <Card.Title>
//                           <FontAwesomeIcon icon={faMobile} size="1x" />{" "}
//                           {reseller.phoneNumber}
//                         </Card.Title>
//                         <Card.Subtitle className="mb-2 text-muted">
//                           Balance: {reseller.balance}
//                         </Card.Subtitle>
//                         <Card.Link>
//                           <Link
//                             to={`${this.props.router.location.pathname}/resellercard/${reseller.phoneNumber}`}
//                           >
//                             Cards
//                           </Link>
//                         </Card.Link>
//                         <Card.Link>
//                           <Link
//                             to={`${this.props.router.location.pathname}/reselleractivity/${reseller.phoneNumber}`}
//                           >
//                             Activity
//                           </Link>
//                         </Card.Link>
//                       </Card.Body>
//                     </Card>
//                   </>
//                 );
//               })}
//               <Pagination>{this.paginationItems()}</Pagination>
//             </Col>
//           </Row>
//         </>
//       );
//     }
//   };

//   getCards = () => {
//     const { loading, balances } = this.props.resellerCardsBalances;

//     return (
//       <>
//         <h2 style={{ textAlign: "center" }}>Reseller Balances</h2>
//         {!loading ? (
//           <>{this.getResellerCards()}</>
//         ) : (
//           <div
//             style={{
//               display: "flex",
//               alignItems: "center",
//               justifyContent: "center"
//             }}
//           >
//             <Spinner animation="border" role="status">
//               <span className="sr-only">Loading...</span>
//             </Spinner>
//           </div>
//         )}
//       </>
//     );
//   };

//   render() {
//     return (
//       <Container>
//         <Switch>
//           <Route
//             exact
//             path="/resellercardsbalances"
//             component={this.getCards}
//           />

//           <Route
//             path="/resellercardsbalances/resellercard/:phone"
//             render={props => (
//               <ResellerCardBalance
//                 data={this.props.resellerCardsBalances.balances}
//                 {...props}
//               />
//             )}
//           />

//           <Route
//             path="/resellercardsbalances/reselleractivity/:phone"
//             render={props => (
//               <UserActivity
//                 data={this.props.resellerCardsBalances.balances}
//                 {...props}
//               />
//             )}
//           />
//         </Switch>
//       </Container>
//     );
//   }
// }

// const mapStateToProps = state => {
//   return {
//     user: state.user,
//     resellerCardsBalances: state.resellerCardsBalances,
//     router: state.router
//   };
// };

// export default connect(mapStateToProps, { getResellersCardsBalances })(
//   ResellerCardsBalances
// );
