/* eslint-disable import/no-named-as-default */
import { NavLink, Route, Switch } from "react-router-dom";

// import FuelSavingsPage from './containers/FuelSavingsPage';
// import HomePage from "./HomePage";
import NotFoundPage from "./NotFoundPage";
import PropTypes from "prop-types";
import React from "react";
import { hot } from "react-hot-loader";
import Challenge from "./Challenge";

// This is a class-based component because the current
// version of hot reloading won't hot reload a stateless
// component at the top-level.

class App extends React.Component {
  render() {
    const activeStyle = { color: "blue" };
    return (
      <div className="pageBody">
        <div style={{ backgroundColor: "#fff" }}>
          <ul>
            <li className="navLink">
              <NavLink exact to="/" activeStyle={activeStyle}>
                LOGO
              </NavLink>
            </li>
            <li className="navLink" className="home-li">
              <NavLink exact to="/" activeStyle={activeStyle}>
                Home
              </NavLink>
            </li>
            <li className="navLink">
              <a href="#">My Profile</a>
            </li>
            <li className="navLink">
              <a href="#">Clients</a>
            </li>
            <li className="navLink">
              <a href="#">Get In Touch</a>
            </li>
          </ul>

          {/* <NavLink exact to="/" activeStyle={activeStyle}>
            Home
          </NavLink>
          {' | '}
          <NavLink exact to="/challange" activeStyle={activeStyle}>
            Redux demo
          </NavLink> */}
        </div>
        <Switch>
          <Route exact path="/" component={Challenge} />

          <Route component={NotFoundPage} />
        </Switch>
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.element,
};

export default hot(module)(App);
