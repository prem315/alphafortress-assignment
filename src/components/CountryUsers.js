import React from "react";
import { connect } from "react-redux";
import { requestAddData, requestRemoveData } from "../actions/challengeActions";

class CountryUsers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
    };
  }
  handleChange = (event, data, i) => {
    const isChecked = event.target.checked;

    if (isChecked === true) {
      this.props.requestAddData(data);
    }
    if (isChecked === false) {
      this.props.requestRemoveData(data);
    }
  };
  render() {
    const { countryUsers } = this.props;

    return (
      <>
        {countryUsers.data.map((countryUser, i) => {
          return (
            <div key={i}>
              <input
                name={countryUser.name}
                type="checkbox"
                onClick={(event) => this.handleChange(event, countryUser, i)}
                checked={countryUser.checked}
              />
              <span>{countryUser.name}</span>
            </div>
          );
        })}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.dataReducer,
  };
};

export default connect(mapStateToProps, { requestAddData, requestRemoveData })(
  CountryUsers
);
