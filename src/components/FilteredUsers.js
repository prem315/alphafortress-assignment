import React from "react";
import { connect } from "react-redux";
import { requestAddData, requestRemoveData } from "../actions/challengeActions";
import cancel from "../cancel.svg";

class FilteredUsers extends React.Component {
  removeUser = (user) => {
    this.props.requestRemoveData(user);
  };
  render() {
    const { data } = this.props.filteredUsers;

    return (
      <div>
        {data.map((item, i) => {
          return (
            <div className="filteredUser" key={i}>
              {item.name}
              <span
                style={{ float: "right" }}
                onClick={() => this.removeUser(item)}
              >
                <img src={cancel} className="cancel" />
              </span>
            </div>
          );
        })}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.dataReducer,
  };
};

export default connect(mapStateToProps, { requestRemoveData })(FilteredUsers);
