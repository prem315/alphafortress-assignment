import { createStore, compose, applyMiddleware } from "redux";
import reduxImmutableStateInvariant from "redux-immutable-state-invariant";
import thunk from "redux-thunk";
import { createBrowserHistory } from "history";
// 'routerMiddleware': the new way of storing route changes with redux middleware since rrV4.
import { connectRouter, routerMiddleware } from "connected-react-router";
import createRootReducer from "../reducers";
import { loadState, saveState } from "./localStorage";

export const history = createBrowserHistory();
const connectRouterHistory = connectRouter(history);

const persistedState = loadState();

function configureStoreProd() {
  const reactRouterMiddleware = routerMiddleware(history);
  const middlewares = [
    // Add other middleware on this line...

    // thunk middleware can also accept an extra argument to be passed to each thunk action
    // https://github.com/reduxjs/redux-thunk#injecting-a-custom-argument
    thunk,
    reactRouterMiddleware
  ];

  const store = createStore(
    createRootReducer(history), // root reducer with router state
    persistedState,
    compose(applyMiddleware(...middlewares))
  );

  store.subscribe(() => {
    saveState({
      user: store.getState().user,
      cards: store.getState().cards,
      phoneNumbers: store.getState().phoneNumbers,
      resellerActivity: store.getState().resellerActivity,
      resellerCardsBalances: store.getState().resellerCardsBalances
    });
  });

  return store;
}

function configureStoreDev() {
  const reactRouterMiddleware = routerMiddleware(history);
  const middlewares = [
    // Add other middleware on this line...

    // Redux middleware that spits an error on you when you try to mutate your state either inside a dispatch or between dispatches.
    reduxImmutableStateInvariant(),

    // thunk middleware can also accept an extra argument to be passed to each thunk action
    // https://github.com/reduxjs/redux-thunk#injecting-a-custom-argument
    thunk,
    reactRouterMiddleware
  ];

  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // add support for Redux dev tools
  const store = createStore(
    createRootReducer(history), // root reducer with router state
    persistedState,
    composeEnhancers(applyMiddleware(...middlewares))
  );

  store.subscribe(() => {
    saveState({
      user: store.getState().user,
      cards: store.getState().cards,
      phoneNumbers: store.getState().phoneNumbers,
      resellerActivity: store.getState().resellerActivity,
      resellerCardsBalances: store.getState().resellerCardsBalances
    });
  });

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept("../reducers", () => {
      const nextRootReducer = require("../reducers").default; // eslint-disable-line global-require
      store.replaceReducer(connectRouterHistory(nextRootReducer));
    });
  }

  return store;
}

const configureStore =
  process.env.NODE_ENV === "production"
    ? configureStoreProd
    : configureStoreDev;

export default configureStore;
