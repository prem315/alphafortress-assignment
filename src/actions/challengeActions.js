export const REQUEST_ALL_DATA = "REQUEST_ALL_DATA";
export const REQUEST_ALL_DATA_FAILED = "REQUEST_ALL_DATA_FAILED";
export const REQUEST_ALL_DATA_SUCCESS = "REQUEST_ALL_DATA_SUCCESS";

export const REQUEST_ADD_DATA = "REQUEST_ADD_DATA";
export const REQUEST_REMOVE_DATA = "REQUEST_REMOVE_DATA";

export const requestData = (data) => {
  return {
    type: REQUEST_ALL_DATA,
    data,
  };
};

export const requestAddData = (data) => {
  return {
    type: REQUEST_ADD_DATA,
    data,
  };
};

export const requestRemoveData = (data) => {
  return {
    type: REQUEST_REMOVE_DATA,
    data,
  };
};
